# 百度搜索爬虫

#### 介绍
百度搜索爬虫，可根据关键字进行爬取相关数据，网址链接

#### 软件架构
软件架构说明
主要是根据搜索的关键字，对全网的网站进行搜索匹配，爬取所需的网站的链接信息，进行mysql数据的存储


#### 安装教程

1.  首先需要vscode，navicat,python3+的本地环境的安装，具体的步骤请看我写的博客：https://blog.csdn.net/weixin_41823246/article/details/126174150?spm=1001.2014.3001.5501

#### 使用说明

1.  注意navicat的账号，密码
2.  navicat启动时，需要运行的环境，可以下载phpstudy的环境
3.  注意数据库中的keyword word搜索时的顺序，位置颠倒的话，可能查询时，结果不精确，具体看我的数据库中存在的案例

#### 参与贡献

1.  具体看我的博客CSDN——————————————骨子里的偏爱



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
