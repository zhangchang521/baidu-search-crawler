/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : baiduresult

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 04/08/2022 14:14:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for keywords
-- ----------------------------
DROP TABLE IF EXISTS `keywords`;
CREATE TABLE `keywords`  (
  `KeywordID` int(11) NOT NULL AUTO_INCREMENT,
  `Keyword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Word` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`KeywordID`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of keywords
-- ----------------------------
INSERT INTO `keywords` VALUES (1, '内燃机', '内燃机');

-- ----------------------------
-- Table structure for keywordslinks
-- ----------------------------
DROP TABLE IF EXISTS `keywordslinks`;
CREATE TABLE `keywordslinks`  (
  `LinkID` int(11) NOT NULL AUTO_INCREMENT,
  `Link` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Content` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `KeyWordID` int(11) NOT NULL,
  PRIMARY KEY (`LinkID`) USING BTREE,
  INDEX `KeyWordID`(`KeyWordID`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of keywordslinks
-- ----------------------------
INSERT INTO `keywordslinks` VALUES (1, '1', '内燃机', 1);

SET FOREIGN_KEY_CHECKS = 1;
